-- table needs datatypes. table needs to know what data each field holds
-- https://www.postgresql.org/docs/9.5/datatype.html
-- https://www.postgresqltutorial.com/postgresql-foreign-key/
-- https://www.postgresql.org/docs/9.5/ddl-constraints.html
-- https://www.postgresql.org/docs/9.3/ddl-default.html
-- https://www.postgresqltutorial.com/postgresql-create-table/
-- https://www.youtube.com/watch?v=EfyG9tcZlXc&list=PLjftwBOk2wf6_KQgqcrA4k3WRVkvG22Tf&index=4
-- https://www.postgresqltutorial.com/postgresql-current_timestamp/
-- https://www.postgresqltutorial.com/postgresql-count-function/
-- https://www.postgresqltutorial.com/postgresql-update/
-- https://www.postgresqltutorial.com/postgresql-order-by/
-- https://www.postgresqltutorial.com/postgresql-describe-table/

-- Query to create a table for twitterusers
-- Jalon explained foreign key is like a placeholder. doesn't hold an actual link to the table
-- I was overthinking it and confusing myself with foreign key

CREATE TABLE twitterusers(
    id SERIAL NOT NULL PRIMARY KEY,
    username VARCHAR(150) NOT NULL,
    password VARCHAR(50) NOT NULL,
    display_name VARCHAR(150)
);


-- Query to create a table for tweets
CREATE TABLE tweets(
    id SERIAL NOT NULL PRIMARY KEY,
    fk_twitteruser INT NOT NULL,
    body VARCHAR(140) NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP

);


-- Query to create a table for notifications
CREATE TABLE notifications(
    id SERIAL NOT NULL PRIMARY KEY,
    fk_twitteruser INT NOT NULL,
    fk_tweet INT NOT NULL

);


-- Query to create a new user (username: steve, password: hunter2, display name: steve-o)
INSERT INTO twitterusers (username, display_name, password) VALUES ('steve', 'steve-o', 'hunter2');


/* Query to create two new users at once: 
 -username: dave, password: asdf, display name: davey
 -username: bob, password: qwer, display name: bobbinator */
 INSERT INTO twitterusers (username, display_name, password) VALUES ('dave', 'davey', 'asdf'), ('bob', 'bobbinator', 'qwer');


 -- Query to get the username and password of twitteruser ID 1
SELECT (username, password) FROM twitterusers WHERE id = 1;


-- Query to get the ID of a user by the username of dave
SELECT id FROM twitterusers WHERE username = 'dave';


-- Query to create a new tweet written by the user with the username steve (yes, you have to get the ID of Steve first -- hint: subqueries are your friend)
INSERT INTO tweets (fk_twitteruser, body) VALUES ((SELECT id FROM twitterusers WHERE username='steve'), 'Hello');


-- Query to get the count of tweets by username steve (hint: subqueries are your friend)
SELECT COUNT(*) FROM tweets WHERE fk_twitteruser=(SELECT id FROM twitterusers WHERE username='steve');


-- Query to get the date and text of all tweets by username steve (hint: subqueries are your friend)
SELECT created_at, body FROM tweets WHERE fk_twitteruser=(SELECT id FROM twitterusers WHERE username='steve');


-- Query to get the username and password of the username bob
SELECT username, password FROM twitterusers WHERE username='bob';


-- Query to create a notification for username bob using the tweet written by username steve (hint: subqueries are your friend)
INSERT INTO notifications (fk_twitteruser, fk_tweet) VALUES ((SELECT id FROM twitterusers WHERE username='bob'),
(SELECT id FROM tweets WHERE fk_twitteruser=(
    SELECT id FROM twitterusers WHERE username='steve')));


-- Query to get all IDs of notifications for bob (hint: subqueries are your friend)
SELECT id FROM notifications WHERE fk_twitteruser = (SELECT id FROM twitterusers WHERE username='bob');

-- EC: 

-- Extra Credit, 1pt: Query to delete a tweet starting when you already know the ID (use 1)
DELETE FROM tweets WHERE id = 1;


-- Extra Credit: 2pts: Single query to delete all the tweets belonging to username bob
DELETE FROM tweets WHERE username ='bob';


/* Extra Credit: 5pts: Single query to output:
    Username of the person the notification is meant for (steve-o)
    username of the person who wrote the tweet that triggered the notification
    text of the tweet
    ...where the only piece of information you start with is the display name of Steve; "steve-o" */
    -- select from where and join maybe...use all 3 tables
-- SELECT (id, username, display_name) FROM twitterusers


-- Query to update Steve's display name to "steviethewonderdude"
UPDATE twitterusers SET display_name='steviethewonderdude' WHERE display_name='steve-o';


-- Single query to update Dave's display name to "DMG" and his password to "iamthedmg"
UPDATE twitterusers SET display_name='DMG', password='iamthedmg' WHERE username='dave';


-- Query to use the ORDER BY attribute to sort the content of the tweets table by newest tweets first (sort on the created_at field)
-- PostgreSQL evaluates the clauses in the SELECT statement in the following order: FROM, SELECT
SELECT * FROM tweets ORDER BY created_at DESC;
