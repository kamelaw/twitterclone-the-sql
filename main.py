# https://docs.python.org/3/library/sqlite3.html
# https://www.sqlite.org/lang_createtable.html
# https://www.sqlitetutorial.net/sqlite-create-table/
# https://sqlite.org/autoinc.html
# https://www.youtube.com/watch?v=girsuXz0yA8
# https://www.sqlite.org/datatype3.html
# https://stackoverflow.com/questions/65937662/foreign-keys-not-working-sqlite3-operationalerror-unknown-column-user-id-in
# https://mimesis.name/
# https://pypi.org/project/mimesis/
# https://mimesis.name/getting_started.html

import sqlite3
import cProfile
from mimesis import Person, Text

# conn short for connection. define connection & cursor
# creates database
conn = sqlite3.connect('deux.db')

print(conn.total_changes)
# cursor interacts with database
cursor = conn.cursor()
# # Create table
# # use if not exists to see if your three twitterclone tables are there
# sqlite3 only has 5 data types. NULL(doesn't exist or does it not exist)
#  INTEGER(whole # 1, 9 etc), REAL(decimal like 10.5), TEXT(text)
# BLOB(image, mp3)

cursor.execute("""CREATE TABLE IF NOT EXISTS twitterusers(
    user_id INTEGER PRIMARY KEY AUTOINCREMENT, 'username' TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL, 'display_name' TEXT NOT NULL)""")

mimesis.Person = Person('en')
i = 500
while i < 500:
    x = Person('en')

cursor.execute("INSERT INTO twitterusers VALUES ('1', 'steve', 'hunter2', 'steve-o')")

print(cursor.execute('SELECT * FROM twitterusers'))


cursor.execute("""CREATE TABLE IF NOT EXISTS tweets(
    tweet_id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER,
    body VARCHAR(140) NOT NULL, created_at DATETIME,
    FOREIGN KEY(id) REFERENCES twitterusers(user_id))""")

cursor.execute("INSERT INTO tweets VALUES ('1', '1', 'My first tweet', '1') ")

cursor.execute('SELECT * FROM tweets')


cursor.execute("""CREATE TABLE IF NOT EXISTS notifications(
    notif_id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER,
    tweet_id INTEGER, FOREIGN KEY(id) REFERENCES twitterusers(user_id),
    FOREIGN KEY(id) REFERENCES tweets(tweet_id))""")

cursor.execute("INSERT INTO notifications VALUES ('', '', '')")

cursor.execute('SELECT * FROM notifications')


results = cursor.fetchall()
print(results)
# commit our command
conn.commit()

# close connection
conn.close()
